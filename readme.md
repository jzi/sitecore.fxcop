Sitecore.FxCop
====================

Custom FxCop rules to check for Sitecore best practices.

Getting started
---------------------

In the Bitbucket project you can either download the source file and compile, or you can go to the download page and get the Sitecore.FxCop.dll file out of the zip file.

Add the Sitecore.FxCop.dll to your FxCop\Rules directory. This will enable FxCop to run these rules every time FxCop is executed. By default, for FxCop 10, this is: C:\Program Files (x86)\Microsoft Fxcop 10.0\Rules.
With Visual Studio 2013, FxCop is installed in C:\Program Files (x86)\Microsoft Visual Studio 12.0\Team Tools\Static Analysis Tools\FxCop\

Alternatively, when FxCop is started, in the Rules tab additional rules can be added by right-clicking on the FxCop project. After this, select the Sitecore.FxCop.dll from where you've downloaded the dll file. 

Debugging / Testing
---------------------

You need Visual Studio 2013 installed to debug the rules inside Visual Studio.

Rules can be debugged inside Visual Studio by running the Sitecore.FxCop.Testing project.
You need to set the path to your Visual Studio / FxCop installation inside Sitecore.FxCop.Testing > Properties > Debug

#### Start external program:
*C:\Program Files (x86)\Microsoft Visual Studio 12.0\Team Tools\Static Analysis Tools\FxCop\FxCopCmd.exe*

#### Command line arguments:
*/file:Sitecore.FxCop.Testing.dll /rule:../../../Sitecore.FxCop/bin/Debug/Sitecore.FxCop.dll /console /directory:lib*

When set up correctly, you can set breakpoints and run the Sitecore.FxCop.Testing project. 

Alternatively, you can call _run-fxcop.cmd_ to test the rules.

Links
---------------------

[Sitecore and Fox Cop Blogpost](http://trayek.wordpress.com/2014/01/07/sitecore-and-fxcop/)

[Shared Source Module](http://trayek.wordpress.com/2014/01/07/sitecore-and-fxcop/)